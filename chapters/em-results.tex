\chapter{Results \& Discussion}

Based on the modified espresso machine with the completed firmware, we can evaluate the temperature control performance. Moreover, we can review the firmware performance and Bern RTOS usability.

%-------------------------------------------------------------------------------
\section{Temperature Control}

The goal set at the beginning of the espresso machine application was to implement a controller that provides repeatable conditions for every coffee preparation. The evaluation is based on the measurement and control states of the machine logs in the database. Additionally, the coffee temperature was measured using an external multimeter (Keysight U1242C).

\begin{figure}[htb!]
    \centering
    \includegraphics[scale=0.9]{2022-06-23_meas_full}
    \caption{Temperature measurement and power input.}
    \label{fig:meas-full}
\end{figure}

The measurement in \autoref{fig:meas-full} shows a stabilization phase for the first 17 min. During which no fresh water was pumped to the boiler allowing for the machine to heat up. The water (red) and boiler (yellow) reach the target temperature within three minutes. However, it takes more than 15 min for the group head to heat up. A slight overshoot in water temperature (red curve) indicates some discrepancy between the simulation and the real world. The heating element is constantly turned on for the first few minutes, as indicated by the power input in the bottom plot. Thus, the controller tries reaching the set point with the maximum speed. When the controller has settled the temperature remains stable.

Following the heat up phase are three espresso shots. The temperature curve during the first two shots is plotted in \autoref{fig:meas-zoom}.

\begin{figure}[htb!]
    \centering
    \includegraphics[scale=0.9]{2022-06-23_meas_zoom}
    \caption{Measured temperatures during espresso preparation.}
    \label{fig:meas-zoom}
\end{figure}

The coffee temperature (blue) curve is the most important indicator of the controller performance. The temperature profile from espresso shots should be within a 2°C margin. Deterministic water temperature reduces the uncertainty of one out of many parameters of coffee preparation. At the beginning of the espresso shot, the temperature within the coffee puck (blue) first drops to 70°C for while the remaining cold water between the boiler and group head is pumped through. Then, hot water from the boiler enters the group head and quickly reaches an equilibrium at 90°C. The short peak before the 20 min mark is due to flushing the group head after the portafilter was removed. Comparing the two espresso shots at the 19 min and 21 min mark proves that the temperature profiles are in fact equal. The raw data from all three espresso preparations revealed that the brew temperature of the coffee puck stayed within 89.3°C and 90.6°C.

The water temperature (red) indicates a stark change in temperature when fresh water enters the boiler. Even more so is the effect on the boiler temperature (yellow). The change in temperature of the boiler might be due to the fresh water supply being screwed onto the boiler. The brass boiler conducts thermal energy quickly. Comparing water (blue) and water (red) temperatures indicates that a change inside the boiler does not immediately affect the coffee temperature. Hence, the thermal model of the espresso machine is forgiving.

Looking at the power input in the bottom plot, it becomes evident that the controller turns the heating element on as soon as the fresh water enters the boiler. The heating element stays on until the temperatures reach a sensible level. Thus, the hardware capabilities limit the temperature control.

Overall the measurements demonstrate that the controller keeps temperatures within the limits set for the application.

%-------------------------------------------------------------------------------
\section{Firmware}

In summary, the final firmware can control actuators based on measurements from RTD temperature, flow and pressure sensors. The display shows live measurements. The user can control the machine using the tactile switches. Measurements are also sent to a computer on the local network for temperature control analysis.

One goal of the application was to implement these features to create a usable prototype machine. However, we should also assess the memory footprint and the performance of the application. Finally, the real-world use case was meant to reveal issues of Bern RTOS, which we will review as well.

%...............................................................................
\subsection{Memory Footprint}

Memory on microcontroller is typically sparse. Hence, an RTOS should be as small as possible for the features it provides. Cargo bloat \cite{github:cargo-bloat} is a tool that groups symbols in the linker output map by crates used in a project. There is some guesswork, as some function might get inlined outside a crate. Using the tool the application was analyzed in debug and release build mode. As an intermediate, there is also a debug mode where all application dependencies are built in release mode. The results are listed in \autoref{tab:memory-footprint}.

\input{tables/flash-sizes.tex}

The binary size varies greatly with optimization level as indicated by the total flash size. The biggest contributor is the GUI. That comes as no surprise as there is an image in flash and graphics libraries are typically large. Many binary symbols from the GUI are not correctly picked up by the tool and are counted towards the \emph{other} section. The release build without GUI resulted in a 66 kB binary. Some crates produce larger binaries in release mode than debug, which might be due to inlining, moving code from one crate to another.

During development, it became evident that optimizing dependencies is necessary in debug mode. Not only because it halves the binary size, but also because it greatly improves timing.

Bern RTOS uses 6.1 kB Flash memory in release mode. Although, the impact on binary size is bigger because some code might be inlined in the application. Additionally, there are macros that generate RTOS related code in the user application.

%...............................................................................
\subsection{Performance}

More important than the impact of an RTOS on the binary size is the real-time performance. Opposed to static size analysis, timing is evaluated at runtime using tracing functions. SEGGER SystemView is one of the few tools for RTOS analysis. The tool is designed to visualize and analyze traces from RTOS in real-time.

\begin{figure}[H]
    \centering
    \begin{overpic}[abs,width=\linewidth,percent,frame]{systemview}
        \put(35,38){\color{red}\circled{1}}
        \put(85,38){\color{red}\circled{2}}
        \put(28,18){\color{red}\circled{3}}
        \put(48,6){\color{red}\circled{4}}
    \end{overpic}
    \caption{SEGGER SystemView application.}
    \label{fig:systemview}
\end{figure}

The screenshot in \autoref{fig:systemview} shows an event trace with cycle timestamp \circled{1}, log messages \circled{2}, a scheduling timeline \circled{3} and usage statistics \circled{4}.

The statistics over a 30s time window are listed in \autoref{tab:systemview-statistics}.

\begin{table}[htb!]
    \center
    \begin{tabular}{ll r@{}r@{}r r@{}r@{}r lr}
        \toprule
        \textbf{Name} &\textbf{Priority} &\multicolumn{3}{l}{\textbf{Run Time / \textmu s}} &\multicolumn{3}{l}{\textbf{Blocked / \textmu s}} &\textbf{Interrupted / \textmu s} &\textbf{Load / \%}\\
        \midrule
        SysTick      &N/A &[2.6, &5.7, &6.1] &N/A & &	&0	&0.58\\
        Scheduler    &N/A &[3.7, &7.5, &12.4] &N/A & &	&0	&1.45\\
        Control      &0   &[4.8, &10.3, &40.9] &[4.3, &5.7, &17.8]	&0	&0.05\\
        Temp Control &1   &[4.9, &79.8, &220.0] &[43.2, &51.6, &80.9]	&0.034	&0.05\\
        Sense        &2   &[3.9, &65.9, &710.0] &[7.6, &11.1, &332.0]	&0.001	&5.32\\
        Network Log  &3   &[13.0, &127.5, &1187.0] &[9.5, &~32.2, &781.0]	&1.93	&1.42\\
        GUI          &5   &[50.0, &~916.0, &~19752.0] &[9.6, &23.7, &~2368.0]	&5.23	&40.52\\
        Idle         &7   &N/A & &	&N/A & &	&N/A	&50.6\\
        \bottomrule
    \end{tabular}
    \caption[CPU usage statistics.]{CPU usage statistics. Arm Cortex-M7 @ 216 MHz. [min, mean, max]}
    \label{tab:systemview-statistics}
\end{table}

The system spends more than 90\% of the time in the GUI thread or in idle. The system overhead from Bern RTOS is 2\%. It is a combination of the SysTick handling system time events and the scheduler switching thread context. On average, it takes 13.2 \textmu s (SysTick and then context switch) from the time a sleeping thread is ready until it runs. In the worst case, it takes 18.5 \textmu s. Because of the scheduler implementation switching to low priority threads takes longer than to higher priority ones.

The \emph{control} thread has the highest priority and runs for just 10.3 \textmu s on average. This thread is rarely ever blocked or interrupted. Hence, the control of the actuators reacts promptly on its inputs.

Converting and reading measurements from the external ADC in the \emph{sense} thread takes a while. First, the measurement channel is selected. The thread then waits until enough measurement are acquired to fill the filter buffer and only then it reads the sample. The ADC is polled during the conversion, resulting in 5.3\% load on the CPU.

Beside the performance analysis, tracing was useful during development. The interaction between threads and issues arising from events can easily be followed. The events that cause a message queue to overflow become evident in the event trace. 

%...............................................................................
\subsection{Bern RTOS API}

When developing an application, the characteristics and problems of an RTOS become clearer than when writing unit tests. The recommended firmware structure for Bern RTOS  is illustrated in \autoref{fig:main-proc}.

\begin{figure}[htb!]
    \centering
    \includegraphics{main-proc.pdf}
    \caption{Firmware module hierarchy and moving resources.}
    \label{fig:main-proc}
\end{figure}

The initialization phase starts when the program enters the main function. At that point, the CPU has access to the entire system. Hence, the board is set up and split to pass it to the different processes. Additionally, inter-process message queues (blue) between processes are allocated. Main then passes these resources to the processes. Processes are loosely coupled by message queues and each implemented in its own module.

A process then allocates synchronization primitives (red) for the threads and passes resources to the threads. Finally, the kernel starts, memory barriers are set up and resources cannot be moved anymore.

This structure splits a complex problem into manageable and loosely coupled problems. Each process has its own memory region with an allocator. Therefore, memory shortages can be traced back to the threads within that process. However, at the moment thread priorities are global. The process level is skipped and priorities are scattered over multiple files. As thread priorities influence the performance of the application greatly, they should be on process level. As a solution a process priority could be introduced. The process priorities could be set in main. Then, on each process level, the priorities for the threads within the process could be set. Hence, no knowledge about the whole application is necessary.

At this moment, the kernel provides the synchronization primitives mutex and semaphore. Using semaphores to flag an event from an interrupt is however more cumbersome than necessary. Often threads only rely on a signal from an event. The kernel should also provide this simple event primitive.

The entry point of a thread is a function that contains some initialization and then an infinite loop. One issue with the infinite loop is that testing is suboptimal because it is more work to input test data into a running loop than to input a function. Another problem is awaiting one of many events. As embedded systems mostly run event based the CPU is often awaiting events. The kernel currently does not support awaiting more than one event at a time. For example, the network log thread polls multiple message queues to check if there is any new data. The kernel API could be remodeled so that the thread entry function signals pending events in its context. Most importantly, the entry function would always run to completion. That solution would simplify testing and event processing.

On testing, there are currently no mocks or simulation of the kernel. Thus, the application can only be tested at the unit level. Integration tests on the host system are impossible. Traits abstracting RTOS dependencies and mocks should be added to the kernel.

Lastly, the API has functions relying on memory allocation and some on statically sized data. The API should be usable with static and dynamic memory allocation. Though, the impact of the allocation scheme should be minimal on the kernel usage. For example, initializing a thread with a statically allocated stack should be almost identical to a dynamically allocated one.
