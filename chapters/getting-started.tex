\chapter{Getting Started}
\label{chap:results}

In this chapter, we will look at a simple example application using the Bern RTOS. This step-by-step introduction showcases some implemented features from a user perspective. The source code for the example application can be found in the example directory of the kernel repository \cite{gitlab:bern-rtos}.

%-------------------------------------------------------------------------------
\section{Toolchain Setup}

The example code is based on the Rust \mintinline{text}{embedded-hal} \cite{github:embedded-hal} and \mintinline{text}{stm32f4xx-hal} \cite{github:stm32f4xx-hal}. If you have not used Rust on a microcontroller before, please see the Embedded Rust Book \cite{rust-embedded:book} on how to set up the Rust toolchain and the basics of embedded Rust development.

For the example application, we will use an STM32F446RE microcontroller with an Arm Cortex-M4F CPU. First, we add the target in \mintinline{sh}{rustup} and change to the nightly version of the compiler.

\begin{customlisting}
\begin{minted}[escapeinside='']{sh}
rustup target add thumbv7em-none-eabihf
rustup default nightly
\end{minted}
\caption{Adjusting the default Rust toolchain from the command line.}
\label{lst:rust-toolchain}
\end{customlisting}

Please note that the nightly compiler is needed only for early kernel versions. The few nightly features in use will be replaced in the future, so that the kernel compiles with the stable version.

There are project templates provided for Bern RTOS based applications. First, we must install some additional tools to create a project (\mintinline{rust}{cargo-generate}) and to flash the target (\mintinline{rust}{probe-run}).

\begin{customlisting}
\begin{minted}[escapeinside='']{sh}
cargo install cargo-generate
cargo install probe-run
\end{minted}
\caption{Installing additional packages from the command line.}
\label{lst:cargo-inst}
\end{customlisting}    

%-------------------------------------------------------------------------------
\section{Configuration}

Now that we are all set, we can create a new project using a template.

\begin{customlisting}
\begin{minted}[escapeinside='']{sh}
cargo generate --git https://gitlab.com/bern-rtos/templates/cortex-m.git
\end{minted}
\caption{Generate a Bern RTOS based project.}
\label{lst:cargo-gen}
\end{customlisting}

The newly generated project resembles any \mintinline{rust}{embedded-hal} based Rust project. However, there are some differences that we will take a look at. First up is the Carog manifest. 

\begin{customlisting}
\listingpath{[project]/Cargo.toml}
\begin{minted}[escapeinside=||]{toml}
[package]
name = "blinky"
version = "0.1.0"
edition = "2021"

[dependencies]
cortex-m-rt = "0.7"
panic-halt = "0.2.0"
bern-kernel = "0.3"|\circled{1}|

# Adjust here and in `.cargo/conf.toml` for your device.
[dependencies.stm32f4xx-hal]|\circled{2}|
features = ["stm32f411"]
version = "0.13"

[profile.dev.package."*"]|\circled{3}|
codegen-units = 1
opt-level = "s"

[patch.crates-io]|\circled{4}|
bern-conf = { path = "conf" }
\end{minted}
\caption{Cargo manifest.}
\label{lst:demo-toml}
\end{customlisting}

In the Cargo manifest in \autoref{lst:demo-toml} the kernel is included at \circled{1}. The kernel selects the architecture support automatically based on the toolchain. This example uses a microcontroller HAL \circled{2}. Be sure to select your microcontroller HAL and change the target in \mintinline{rust}{[project]/.cargo/conf.toml} accordingly.

Building Rust applications in debug mode leads to much larger binaries than in release mode. This affects the performance of the RTOS as well. In order to use debug symbols and compromise little on performance, we can enable optimization on the dependencies only \circled{3}.

Bern RTOS needs additional configuration values outside the Cargo manifest. This is done using a crate in the user application that replaces the default one. This patch is specified at \circled{4}.

\begin{customlisting}
\listingpath{[project]/conf/conf.rs}
\begin{minted}[escapeinside='']{rust}
pub const CONF: Conf<0> = Conf {
    kernel: Kernel'\circled{1}'{
        priorities: 8,
        memory_size: Byte::from_kB(2),
    },

    shared: Shared'\circled{2}'{
        size: Byte::from_kB(1),
    },

    memory_map: MemoryMap'\circled{3}'{
        flash: Memory {
            link_name: "FLASH",
            start_address: 0x0800_0000,
            size: Byte::from_kB(512),
        },
        sram: Memory {
            link_name: "RAM",
            start_address: 0x2000_0000,
            size: Byte::from_kB(128),
        },
        peripheral: Memory {
            link_name: "",
            start_address: 0x4000_0000,
            size: Byte::from_MB(512),
        },
        additional: [],
    },

    data_placement: DataPlacement'\circled{4}'{
        kernel: "RAM",
        processes: "RAM",
        shared: "RAM"
    }
};
\end{minted}
\caption{Bern kernel configuration.}
\label{lst:kernel-conf}
\end{customlisting}

\autoref{lst:kernel-conf} starts with the kernels internal parameters \circled{1}. By default, there are 8 priorities and 2kB reserved for the kernel. When libraries use static memory they will be placed in a shared section instead of a process. How much shared memory is accessible can be set at \circled{2}. This value is preferably kept at a minimum.

Memory protection is currently used to restrict access to stacks within one process only. Access to the flash memory and peripherals is granted to each thread. For the kernel to set appropriate memory protection rules, some memory addresses must be specified \circled{3}.

Some microcontrollers offer faster or external memory. We can select where to put the data structures at \circled{4}.

%-------------------------------------------------------------------------------
\section{Process \& Threads}

Bern RTOS uses processes to split an application into loosely coupled parts. A process contains one or more threads. The threads can access all memory within a process, i.e., other tasks stack, the process static memory and the process allocator. However, a thread can only pass messages to a thread running in another process via the kernel.

\begin{customlisting}
\listingpath{[project]/main.rs}
\begin{minted}[escapeinside='']{rust}
static PROC: &Process = bern_kernel::new_process!(my_process'\circled{1}', 8192);

#[entry]
fn main() -> ! {'\circled{2}'
    let mut board = Board::new();

    bern_kernel::kernel::init();'\circled{3}'
    bern_kernel::time::set_tick_frequency(1.kHz(), board.sysclock().Hz());

    let mut led = board.led;
    PROC.init(move |c| {'\circled{4}'
        Thread::new(c)'\circled{5}'
            .priority(Priority::new(0))
            .stack(Stack::try_new_in(c, 1024).unwrap())
            .spawn(move || {
                loop {'\circled{6}'
                    led.set_high();
                    sleep(250);
                    led.set_low();
                    sleep(750);
                }
            });
    }).unwrap();

    bern_kernel::start();'\circled{7}'
}
\end{minted}
\caption{Blinky example using Bern RTOS.}
\label{lst:example-main}
\end{customlisting}

In this example we will just use one process and one thread. A new process is created at \circled{1}. The name given is used to crate a linker section where we can put static variables into. The second parameter specifies the process memory size in bytes. Process memory contains static data and dynamically allocated data, which contains the thread stacks.

Bern RTOS is a library OS, which means that the user is responsible for the boot process and starting the kernel. In this case we use the \mintinline{rust}{cortex-m-rt} crate to get to \mintinline{rust}{main} \circled{2}. Then, the hardware is initialized.

Before any interaction with the RTOS, we have to initialize the kernel \circled{3} and set the tick frequency. A tick is the time base for the kernel. In most applications, one tick should take 1 ms, i.e., a tick frequency of 1 kHz. As the kernel API calls are millisecond-based understanding time values is simple.

Threads can only be created inside a process. Process initialization is started at \circled{4}. Using the process context, we can create a new thread \circled{5}. The default priority is changed to zero, the highest priority. Next, we allocate a stack for the thread to run on. Finally, we write the entry function for the thread \circled{6}. It contains an infinite loop as the thread should run forever. The \mintinline{rust}{spawn} method uses a closure so that resources are captured from the environment without having to specifically pass them to a thread.

Now that the system is completely initialized, we can start the kernel \circled{7}.

%-------------------------------------------------------------------------------
\section{Synchronization}

So far the example implements a blinking LED in one thread. Let us now extend the application such that the delay time can be adjusted using a button on the board. The value will be adjusted in an interrupt service routing (ISR) and shared with the blinky thread. The thread and the ISR run in different contexts pseudoparallel. Thus, access to the shared delay must be synchronized to prevent inconsistent data.

\begin{customlisting}
\listingpath{[project]/main.rs}
\begin{minted}[escapeinside='']{rust}
let mut led = board.led;
let mut button = board.button;
PROC.init(move |c| {
    let delay = Arc::new(Mutex::new(100_u32));'\circled{1}'

    let delay_reader = delay.clone();
    Thread::new(c)
        .stack(Stack::try_new_in(c, 1024).unwrap())
        .spawn(move || {
            let mut local_delay = 100;
            loop {
                led.set_high();
                sleep(200);
                led.set_low();
                delay_reader.try_lock()'\circled{2}'
                    .map(|s| local_delay = *s).ok();
                sleep(local_delay);
            }
        });

    InterruptHandler::new(c)'\circled{3}'
        .stack(InterruptStack::Kernel)
        .connect_interrupt(stm32f4xx_hal::interrupt::EXTI15_10 as u16)'\circled{4}'
        .handler(move |_c| {
            button.clear_interrupt_pending_bit();
            delay.try_lock()'\circled{5}'
                .map(|mut s| {
                    *s = if *s < 1000 { *s + 100 } else { 100 };
                }).ok();
        });
}).unwrap();
\end{minted}
\caption{Process initialization with interrupt handler and synchronization.}
\label{lst:example-ipc}
\end{customlisting}

We use a mutex to synchronize access to the shared delay value. The mutex is allocated on the process heap at \circled{1} in \autoref{lst:example-ipc}. The delay value is initially set to 100 and encapsulated in the mutex. The blinky loop in the thread tries locking the mutex \circled{2}. If that succeeds, the local delay values are updated.

Setting up an interrupt handler is similar to a thread \circled{3}. The interrupt number provided by the hardware manufacturer connects the handler to the interrupt \circled{4}. When the button is pressed to the handler is called, which clears the remaining interrupt flags. Then, the shared delay value is locked \circled{5} and incremented. In contrast to a thread an interrupt handler does not contain a loop and is kept as short as possible.

This example illustrates the basic usage of the Bern RTOS kernel. The next chapter explains the design and usage of all kernel components.
