\chapter{Introduction}

In this part of the project, Bern RTOS is put to test in a real-world scenario. The goal is to check whether the RTOS provides all the necessary features to write an example application and if new issues arise from a complex application. The interaction with the kernel will only be a small portion of the implementation. Most code will be based on open source libraries, i.e. hardware abstraction layer. Thus, we will be able to assess how well Rust is suited for embedded software development and the state of the embedded Rust ecosystem.



%-------------------------------------------------------------------------------
\section{Starting Point}

While the example application required complexity to test all parts of the kernel, the target application had to fit the time limitations of the project. For that purpose, an espresso machine controller was chosen. It requires little domain-specific knowledge to understand the problem.

\autoref{fig:em-no-mod} shows a diagram of an espresso machine with a thermostat. The boiler heats the water and keeps it at a constant temperature. When the brew switch is flipped, cold water is pumped from the water reservoir into the boiler. That creates pressure on the water in boiler, which is pushed into the group head and through the coffee puck. Most home espresso machines are built using electromechanical components only, reducing cost and complexity.

\begin{figure}[htb!]
    \centering
    \includegraphics{espresso-no-mod.pdf}
    \caption{Overview of an espresso machine with thermostat.}
    \label{fig:em-no-mod}
\end{figure}

The main task of an espresso machine is to provide the user with water at a given temperature, typically 92..96°C. Some machines use PID control to achieve constant water temperature, but most machines use a bang-bang controller in the form of a thermostat. The latter for example turns the heater on when the temperature is below 90°C and turns off above 95°C. Thermostats are inexpensive, but the water temperature can vary depending on the hysteresis of the thermostat.
The espresso machine used for the example application is a Rancilio Sivlia, which uses a thermostat. The temperature swing in \autoref{fig:temp-silvia} shows that the brew water reaches temperatures above 115°C and below 88°C, which affect the quality of the espresso notably.

\begin{figure}[htb!]
    \centering
    \includegraphics{temp-silvia.pdf}
    \caption{Water temperature inside the boiler of the Rancilio Silvia espresso machine.}
    \label{fig:temp-silvia}
\end{figure}


%-------------------------------------------------------------------------------
\section{Concept}

To achieve the afore-mentioned temperature stability, a new controller will be implemented. For that purpose, new electronics and sensors will replace the original wiring. An overview of the modified machine is shown in \autoref{fig:em-overview}.

\begin{figure}[htb!]
    \centering
    \includegraphics{concept-reg.pdf}
    \caption{Overview of the espresso machine hardware with new control electronics.}
    \label{fig:em-overview}
\end{figure}

The four temperature sensors (\emph{T} in \autoref{fig:em-overview}) are used to identify the system behavior. The goal is to measure temperatures at points in the systems which are normally omitted due to implementation cost. In particular, will the water temperature inside the boiler and the brew temperature of the coffee puck be measured. The thermostat of the unmodified machine was based only on the temperature of the boiler outside. In addition to the temperature sensors, a flow meter (\emph{F}) and manometer (\emph{P}) will be added. These are used to provide feedback on the brew to the user during espresso shots.

A temperature controller of an espresso machine alone does not justify the use of an RTOS. Thus, a high background load is introduced in the form of a graphical user interface (UI). The touch screen will allow the user to configure the machine and display live measurements. Additionally, there will be an ethernet connection for data logging representing a uncertain load arising from connectivity to external devices.

All in all, the example application will feature a critical set of threads responsible for controlling the machines actuators, a background load updating the UI and dynamic load from connectivity.
