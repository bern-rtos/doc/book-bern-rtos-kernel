\chapter{Testing}
\label{chap:testing}

The stability of software can only be evaluated with rigorous testing. Tests are best carried out automatically or manually following a test protocol. Especially, automated regression testing provides safety so that software changes do not break the existing implementation.

In accordance with the software requirements specifications (SRS) and V-model, the kernel is tested on the unit, integration and system level.

%-------------------------------------------------------------------------------
\section{Unit Tests}

Modules are tested in isolation with unit tests. Dependencies on other modules must be replaced by mocks or stubs. For unit tests, Rusts integrated test framework \cite[~178]{blandy_orendorff_2017} is used.

Unit tests are mostly used for standalone modules like linked lists, queues and channels. Some modules are too tightly coupled and need  refactoring for effective unit testing.

%-------------------------------------------------------------------------------
\section{Hardware Integration Tests}

There is no guarantee that the software works as expected if all unit tests succeed. There can be errors in the interfaces between software modules or software and hardware. Especially on embedded systems, the configuration of hardware can be problematic. Hardware integration tests are run against the target hardware and validate the behavior of multiple modules together.

The kernel uses hardware integration tests because a real-time kernel contains critical architecture-dependent code (e.g., context switching or system calls) with assembly code that can only be tested on the correct architecture. Microcontrollers also have hardware to accelerate the code execution in the form of direct memory access (DMA) unit or caching, the effects of which can only be observed on hardware.

A binary for a microcontroller cannot be run on a desktop computer directly, but it can be emulated. QEMU \cite{qemu} is an emulator suitable for microcontroller architectures. The \mintinline{text}{cortex-m} crate uses QEMU to run emulated tests on the Arm Cortex-M3 architecture. Unfortunately,  memory protection unit (MPU) is not supported \cite{zephyr:qemu}, which the kernel heavily relies on. Thus, hardware integration tests are run on actual hardware.

Without the standard library, there is no test framework. Luckily, \mintinline{text}{defmt-test} \cite{github:defmt-test} provides a framework that is similar to the standard library but can run on a microcontroller. However, the framework only runs on the Arm Cortex-M architecture and communication only supports the \mintinline{text}{detfmt} crate. To overcome this issue, \mintinline{text}{defmt-test} was forked into \mintinline{text}{bern-test} and adapted to support any architecture and any serial communication interface. The API was changed to be even more similar to the standard library syntax.

\begin{customlisting}
\listingpath{bern\_kernel/tests/arm\_cm4/tests/mutex.rs}
\begin{minted}[escapeinside='']{rust}
#![no_main]
#![no_std]
/*uses*/

#[bern_test::tests]'\circled{1}'
mod tests {
    /*uses*/

    #[test_set_up]'\circled{2}'
    fn init_scheduler() {
        bern_kernel::init();
        time::set_tick_frequency(1.kHz(), 72.MHz());
    }

    #[test_tear_down]'\circled{3}'
    fn reset() {
        cortex_m::peripheral::SCB::sys_reset();
    }

    #[tear_down]'\circled{4}'
    fn stop() {
        cortex_m::asm::bkpt();
    }

    #[test]'\circled{5}'
    fn wait_for_lock(_board: &mut Board) {
        let mutex = Arc::new(Mutex::new(MyStruct{ a: 42 }));
        PROC.init(move |c| {
            Thread::new(c)
                .priority(Priority::new(1))
                .stack(Stack::try_new_in(c, 1024).unwrap())
                .spawn(move || {
                    match mutex.lock(1000) {
                        Ok(value) => { assert_eq!'\circled{6}'(value.a, 42); },
                        Err(_) => panic!'\circled{7}'("Did not wait for mutex"),
                    }
                });

            // watchdog
            Thread::new(c)
                .priority(Priority::new(0))
                .stack(Stack::try_new_in(c, 1024).unwrap())
                .spawn(move || {
                    sleep(1000);
                    /* if the test does not fail within x time it succeeded */
                    bern_test::test_succeeded();'\circled{8}'
                    __tear_down();
                });
        }).ok();

        bern_kernel::start();
    }
}
\end{minted}
\caption{Hardware integration test of a semaphore.}
\label{lst:tests-hw}
\end{customlisting}

In \autoref{lst:tests-hw} a mutex is tested on hardware. A test module is first marked with the \mintinline{rust}{bern_test::tests} macro \circled{1}. In contrast to the standard library test framework, a setup and tear down function can be specified for the test runner. The test setup function \circled{2} runs before every single test, but it is not mandatory. In this case, the set-up function initializes the scheduler. After each test, a tear down function is called \circled{3}. Here, the CPU is reset between every test. Lastly, after all tests, another tear down function \circled{4} is executed that halts the CPU.

The test itself \circled{5} uses assert \circled{6} and panic \circled{7} from the Rust core library. The board initialization and test runner launch were omitted here, but not that a structure can be passed to the test launcher that will be passed on to every test. If a test does not panic within certain amount of time (e.g. 100 ms) the watchdog thread will end the test and return a success message \circled{8}.

%-------------------------------------------------------------------------------
\section{System Tests}

The system tests validate the behavior of a complete system with other components. In terms of embedded systems, the end product consisting of hardware and software is tested against communication interfaces or electrical signals.

The kernel influences system tests with a computational overhead that affects timing constraints. For example, interrupt handling in the kernel increases latency compared with bare-metal applications. The timing behavior is evaluated using a digital signal generator and analyzer.

\begin{figure}[H]
    \centering
    \includegraphics{integration-testing.pdf}
    \caption{Integration testing setup for timing measurements.}
    \label{fig:tracing}
\end{figure}

The test setup in \autoref{fig:tracing} is built from a STMicroelectronics STM32F446 (Arm Cortex-M4F) microcontroller and a Digilent Digital Discovery logic analyzer/pattern generator. The pattern generator triggers an event on the microcontroller. In reaction the device under test outputs a signal. Depending on the test case, the logic analyzer can measure the time between the stimulus and the reaction signal (e.g. interrupt latency) or the duration the reaction signal was high (e.g. context switch).

A python script on a computer controls the testing procedure. The script sets up the measurement device, programs the microcontroller using cargo and then reads the measurements. The results are stored as raw data for statics analysis and compared against timing requirements. Thanks to pytest, the results can be easily reported to a continuous integration server.

%-------------------------------------------------------------------------------
\section{Results}

Software tests are meant to run automatically on every code change, so that breaking changes are caught as soon as possible. The testing results are often not visible to the end user. The following sections present a snapshot of the implemented tests at the time this document is published.

%...............................................................................
\subsection{Unit Tests}

\autoref{lst:test-res-unit} shows the test results for the kernel unit test. These tests were run on the host system.

\begin{customlisting}
\begin{minted}{text}
test alloc::bump::tests::overflow ... ok
test alloc::bump::tests::alloc_and_dealloc ... ok
test alloc::const_pool::tests::drop_item ... ok
test alloc::heap::tests::only_allocations ... ok
test alloc::heap::tests::alloc_and_dealloc ... ok
test alloc::pool::tests::alloc_and_dealloc ... ok
test alloc::heap::tests::over_allocation - should panic ... ok
test alloc::pool::tests::one_partition ... ok
test alloc::pool::tests::multiple_partitions ... ok
test mem::linked_list::tests::find_and_take ... ok
test mem::linked_list::tests::insert_at_condition ... ok
test mem::linked_list::tests::iterate ... ok
test mem::linked_list::tests::iterate_mut ... ok
test mem::linked_list::tests::length ... ok
test mem::linked_list::tests::memory_overflow ... ok
test mem::linked_list::tests::one_node ... ok
test mem::linked_list::tests::pushing_and_popping ... ok
test mem::queue::mpmc_linked::tests::fifo ... ok
test mem::queue::mpmc_linked::tests::lifo ... ok
test sync::mpsc::tests::by_ref ... ok
test sync::mpsc::tests::queue_overflow ... ok
test sync::mpsc::tests::single_producer ... ok
test sync::mpsc::tests::static_channel ... ok
test sync::spsc::tests::by_ref ... ok
test sync::spsc::tests::overflow ... ok
test sync::spsc::tests::single_producer ... ok
test sync::mpsc::tests::multi_producer ... ok
test sync::spsc::tests::static_channel ... ok
test sync::spsc::tests::underflow ... ok
test sync::spsc::tests::spsc_thread ... ok
\end{minted}
\caption{Kernel unit tests results.}
\label{lst:test-res-unit}
\end{customlisting}

The listing shows that allocators, lists and queues are tested at the unit level. These components manipulate raw memory. Because they contain many unsafe sections, the compiler cannot guarantee correctness. Safe behavior is validated using these unit tests. Additionally, these standalone components do not interact with the kernel. Therefore, they are easy to test.

%...............................................................................
\subsection{Hardware Integration Tests}

The following test scenarios were run against an Armv7E-M based microcontroller (STM32F446RE).

\begin{enumerate}[nosep]
    \item Thread:
    \begin{enumerate}[nosep]
        \item Creating a thread.
        \item Capturing resources from the environment.
        \item Putting a thread into sleep mode.
    \end{enumerate}
    \item Memory Protection:
    \begin{enumerate}[nosep]
        \item Accessing data in the parent process.
        \item Accessing peripherals.
        \item Preventing stack overflow.
        \item Preventing access to memory outside the parent process.
    \end{enumerate}
    \item Mutex:
    \begin{enumerate}[nosep]
        \item Manipulating data stored in a mutex.
        \item Awaiting a mutex.
        \item Behavior on priority inversion.
    \end{enumerate}
    \item Semaphore:
    \begin{enumerate}[nosep]
        \item Manipulating a semaphore.
        \item Awaiting a semaphore.
    \end{enumerate}
\end{enumerate}

These tests focus on the correct behavior of the kernel in case that a thread violates a memory boundary. Correct scheduling of the threads was tested implicitly. As mentioned in the unit test section, testing the interacting components requires more effort. Therefore, the event system used for semaphores and mutexes is tested at the hardware integration level instead.

%...............................................................................
\subsection{System}

The timing results can be either checked against fixed timing requirements or returned as continuous values. The statistics for three test cases are shown in \autoref{tab:timing-stats}. For those tests, the microcontroller was running at 168 MHz.

\input{tables/timing-stats.tex}

Two tests are interrupt driven. The first one uses an interrupt service routine without kernel interaction. The second one uses the kernel interrupt handler. As expected is the direct ISR call was faster than the kernel handler. Though, in release mode the difference is only three fold instead of 10 fold in debug mode. Overall, the release mode improves performance by a factor of 10 to 20. Additionally, jitter is very low, which might be due to the low system load.

The last test case evaluates the time it takes to switch from a low priority thread releasing a semaphore to a high priority thread awaiting the semaphore. In \cite{mipi:timing-comparison} the same test was carried out on an STM32F407 running at 168 MHz with RTOSs written in C. The results are comparable as the microcontrollers are almost identical and they compiled their application with optimization level O3. Bern RTOS (3.8 \textmu s) performs well compared to FreeRTOSv10 (4.13 \textmu s), \ucos-III (5.56 \textmu s) and RTX (3.26 \textmu s). For a final verdict the tests with the RTOS written in C should be repeated to ensure equivalent implementation. Especially, as Bern RTOS should be slightly slower than the other RTOS because it must change the memory protection setting in the context switch. 