MAIN=Bern_RTOS_Kernel.tex
LATEX=pdflatex -shell-escape

increment:
	$(LATEX) $(MAIN)

bibliography:
	biber $(MAIN:.tex=)

release:
	$(LATEX) $(MAIN)
	biber $(MAIN:.tex=)
	$(LATEX) $(MAIN)

.DEFAULT_GOAL := increment
