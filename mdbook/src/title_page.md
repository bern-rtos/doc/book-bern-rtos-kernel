# Bern RTOS Kernel

<img alt="Title Image" style="display:inline" src="img/title-image.svg"/>

**`bern-kernel` version: 0.3.0**

**There are multiple issues with the online book version. Please use the [PDF](https://gitlab.com/bern-rtos/doc/book-bern-rtos-kernel/-/jobs/artifacts/main/raw/Bern_RTOS_Kernel.pdf?job=latex) for now.**

## Authors

- Stefan Lüthi

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" rel="dct:type">work</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## PDF Version

A PDF version of this book can be downloaded [here](https://gitlab.com/bern-rtos/doc/book-bern-rtos-kernel/-/jobs/artifacts/main/raw/Bern_RTOS_Kernel.pdf?job=latex).
